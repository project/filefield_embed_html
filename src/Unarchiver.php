<?php

namespace Drupal\filefield_embed_html;

class Unarchiver {

  /**
   * The file to be processed.
   */
  protected $file;

  /**
   * Constructs the HTMLExtractor object.
   */
  public function __construct($file) {
    $this->file = $file;
  }

  /**
   * Gets the file's MIME Type.
   */
  public function getMimeType($file) {
    return mime_content_type($file);
  }

  /**
   * Extracts the archive into a specified destination directory.
   */
  public function extractTo($destination) {
    if ($this->getMimeType($this->file) == 'application/zip') {
      $archive = new \ZipArchive;

      if ($archive->open($this->file)) {
        for ($i = 0; $i < $archive->numFiles; $i++) {
          $entry = $archive->getNameIndex($i);

          if (preg_match('/\bindex\.html\b/', $entry)) {
            $entries = explode('/', $entry);
            $sub_dir = $entries[0];

            if ($sub_dir === 'index.html') {
              if ($archive->extractTo($destination) !== TRUE) {
                throw new \Exception('Unable to extract the archive file.');
              }
            }
            else {
              $files = array();
              mkdir($destination);

              for ($i = 0; $i < $archive->numFiles; $i++) {
                $entry = $archive->getNameIndex($i);
                $entry_explode = explode('/', $entry);

                if ($entry_explode[0] === $sub_dir && $entry_explode[1] !== '') {
                  $files[] = $entry;
                }
              }

              $temp_dir = file_directory_temp() . $destination;
              if ($archive->extractTo($temp_dir, $files) === TRUE) {
                $this->renameFiles($temp_dir . '/' . $sub_dir, $destination);
              }
              else {
                throw new \Exception('Unable to extract the archive file.');
              }
            }

            break;
          }
        }

        $archive->close();
      }
      else {
        throw new \Exception('The provided file looks invalid.');
      }
    }
    else {
      throw new \Exception('The provided file is not supported. Please use a ZIP archive instead.');
    }
  }

  /**
   * Rename files recursively.
   */
  private function renameFiles($dir, $destination) {
    $files = scandir($dir);

    foreach ($files as $name) {
      if ($name == '.' || $name == '..') {
        continue;
      }

      if (is_dir($dir . '/' . $name)) {
        mkdir($destination . '/' . $name);
        $this->renameFiles($dir . '/' . $name, $destination . '/' . $name);
      } else {
        $oldName = $dir . '/' . $name;
        $newName = $destination . '/' . $name;
        rename($oldName, $newName);
      }
    }
  }

}